import serial, httplib, urllib

def sendToThingspeak(hum, temp, concP1, concP2):

	params = urllib.urlencode({"field1":hum, "field2":temp, "field3":concP1, "field4":concP2, "key":apiKey})
	conn.request("POST", "/update", params, headers)

	response = conn.getresponse()
	print "Sent: %s / %s / %s / %s: " % (hum, temp, concP1, concP2), response.status, response.reason
	response.read()
	
def sendToFile(hum, temp, concP1, concP2):
	
	f = open('sensor_data.txt', 'a');
	f.write(hum + ',' + temp + ',' + concP1 + ',' + concP2 + ';\n');
	f.close();
	

# HTTP Configuration
headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
conn = httplib.HTTPConnection("api.thingspeak.com:80")
apiKey = "6AEGT8V0J3ZS77S8"

# Serial Configuration
serialPort = "COM4"

## Serial & Thingspeak Connection ##
arduino = serial.Serial(serialPort, 9600)
conn = httplib.HTTPConnection("api.thingspeak.com:80")

i = 1;

while(True):
	
	print "Iter no %d" % (i)

	reading = arduino.readline()
	print reading
	reading = reading.replace("\n","")
	hum, temp, concP1, concP2 = reading.split(",", 3);
	
	#sendToThingspeak(hum, temp, concP1, concP2);
	sendToFile(hum, temp, concP1, concP2);
	
	print hum + ',' + temp + ',' + concP1 + ',' + concP2 + ';\n'; 
	
	i += 1;


conn.close()