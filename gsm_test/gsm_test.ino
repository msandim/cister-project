#include "SIM900.h"
 
#include <SoftwareSerial.h>
 
#include "inetGSM.h"
 
InetGSM inet;
 
char msg[1], data;
boolean found=false, started=false;
 
int numdata, i=0;

void SIM900Power()
{
  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH);
  delay(600);
  digitalWrite(9, LOW);
  delay(5000);
}

/*
void flushSIMbuffer()
{
  char response;
  
  do
  {
    response = gsm.read();
    Serial.print(response);
  } while(response > 0);
}
*/

boolean waitHttpOK()
{  
  char okStatus[] = "200 OK";
  int k=0, timeout_value=0;

  while(timeout_value < 500)
  {
    char char_response = gsm.read();

    if (char_response > 0) 
    {
      Serial.print(char_response);

      if(char_response == okStatus[k])
      {
        k++;

        if(k+1 == strlen(okStatus))
        {
          inet.disconnectTCP();
          return true;
        }
      }

      else
        k=0;
    }
  }
  
  inet.disconnectTCP();
  return false;
}
 
void setup()
{
 
Serial.begin(9600);
 
Serial.println("GSM Shield testing.");

//SIM900Power();

Serial.println("Power achieved");
 
if (gsm.begin(9600)){
 
  Serial.println("\nstatus=READY");
 
  started=true;  
 
}
 
else Serial.println("\nstatus=IDLE");
 
if(started){
 
  if (inet.attachGPRS("umts", "", ""))
 
    Serial.println("status=ATTACHED");
 
  else Serial.println("status=ERROR");
 
  delay(1000);
 
  //numdata=inet.httpPOST("www.evildeejay.it", 80, "/test/test.php", "name=Miguew&age=20",msg, 50);
  
  numdata=inet.httpPOST("api.thingspeak.com", 80, "/update", "key=6AEGT8V0J3ZS77S8&field1=50&field2=40&field3=400",msg, 50);
  Serial.println("\n--------- HTTP OK ----------");
  
  if (!waitHttpOK())
    Serial.println("Error waiting for HTTP OK");
    
  Serial.println("\n--------- /HTTP OK/ --------");
  
  delay(15000);
  
  numdata=inet.httpPOST("api.thingspeak.com", 80, "/update", "key=6AEGT8V0J3ZS77S8&field1=20&field2=10&field3=10",msg, 50);
  Serial.println("\n--------- HTTP OK ----------");
  
 if (!waitHttpOK())
    Serial.println("Error waiting for HTTP OK");
    
  Serial.println("\n--------- /HTTP OK/ --------");
  
  delay(15000);
  
  numdata=inet.httpPOST("api.thingspeak.com", 80, "/update", "key=6AEGT8V0J3ZS77S8&field1=30&field2=50&field3=1",msg, 50);
  Serial.println("\n--------- HTTP OK ----------");
  
  if (!waitHttpOK())
    Serial.println("Error waiting for HTTP OK");
    
  Serial.println("\n--------- /HTTP OK/ --------");
  Serial.println("\nFIMM");
}
 
};
 
void loop()
{
};
