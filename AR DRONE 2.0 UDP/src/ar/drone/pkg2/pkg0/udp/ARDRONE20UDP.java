package ar.drone.pkg2.pkg0.udp;

import java.net.*;
import java.util.concurrent.TimeUnit;

public class ARDRONE20UDP {

    public static void main(String[] args) {

        int seq = 1;

        while (true) {
            try {

                DatagramSocket clientSocket = new DatagramSocket();
                InetAddress IPAddress = InetAddress.getByName("192.168.1.1");
                byte[] sendData = new byte[1024];

                String sentence = "AT*REF=" + (seq++) + "," + Integer.toBinaryString(512) + "\r";

                sendData = sentence.getBytes();
                System.out.println(sentence);
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 5556);
                clientSocket.send(sendPacket);
                
                TimeUnit.NANOSECONDS.sleep(62500000);
            } catch (Exception ex) {
            }
        }

    }
}
